package simple_examples;

import helpers.DB_helpers;
import helpers.Selenium_setup;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.GoogleHomePage;

import java.sql.ResultSet;

public class simple1 extends Selenium_setup {

    GoogleHomePage googleHomePage = new GoogleHomePage(driver);

    @Test
    public void simpleTest() {
        String searchText = "test automation framework";
        String sqlQuery = "select * from authors";
        ResultSet rs = null;
        googleHomePage = PageFactory.initElements(driver, GoogleHomePage.class);
        DB_helpers db_helpers = new DB_helpers();

        db_helpers.selectQuery(sqlQuery);

        googleHomePage.navigate_to_google();
        Assert.assertEquals(googleHomePage.isGoogleLogoPresent(), true, "Google logo was not present");
        googleHomePage.regular_search();
        Assert.assertEquals(searchText, googleHomePage.main_search_field.getAttribute("value"),"Google search text didn't match");
        googleHomePage.navigate_to_google();
        googleHomePage.feeling_lucky();
        Assert.assertEquals(googleHomePage.isLuckyYouIMDBPageLoaded(), true, "IMDB page with film Lucky You wasn't loaded as expected");

    }
}
