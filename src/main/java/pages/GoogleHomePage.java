package pages;

import helpers.WebDriver_helpers;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleHomePage extends WebDriver_helpers {

    WebDriver driver;

    public GoogleHomePage(WebDriver driver) {
        this.driver=driver;
    }

    WebDriver_helpers webDriver_helpers = new WebDriver_helpers();

    @FindBy(id = "lst-ib")
    public WebElement main_search_field;

    @FindBy(name = "btnK")
    public WebElement google_search_button;

    @FindBy(name = "btnI")
    public WebElement feeling_lucky_button;

    public String googleLogo = "hplogo";

    public String luckyYouIMDBURL = "https://en.wikipedia.org/wiki/Lucky_You_(film)";

    public void navigate_to_google() {
        driver.navigate().to("http://google.com");
    }

    public void regular_search() {
        main_search_field.click();
        main_search_field.sendKeys("test automation framework");
        main_search_field.sendKeys(Keys.TAB);
        google_search_button.click();
    }

    public void feeling_lucky() {
        main_search_field.sendKeys("lucky you film");
        main_search_field.sendKeys(Keys.TAB);
        feeling_lucky_button.click();
    }

    public boolean isGoogleLogoPresent() {
        if (webDriver_helpers.isElementPresent(driver, By.id(googleLogo))) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isLuckyYouIMDBPageLoaded() {
        if (driver.getCurrentUrl().equalsIgnoreCase(luckyYouIMDBURL)) {
            return true;
        } else {
            return false;
        }
    }
}
