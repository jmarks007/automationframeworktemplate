package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class Selenium_setup {

    public static WebDriver driver;
    public Logger log = Logger.getLogger(getClass().getName());
    String OS = System.getProperty("os.name").toLowerCase();
    public WebDriver createDriver(String browser) {
        if (browser.equalsIgnoreCase(("Chrome"))) {
            ChromeOptions options = new ChromeOptions();
            Map<String, Object> prefs = new HashMap<>();
            prefs.put("profile.default_content_settings.popup", 0);
            prefs.put("credentials_enable_service", false);
            prefs.put("profile.password_manager_enabled", false);
            options.setExperimentalOption("prefs", prefs);
            options.addArguments("--disable-web-security");
            options.addArguments("--test-type");
            options.addArguments("--disable-popup-blocking");
            options.addArguments("disable-infobars");
            options.addArguments("start-maximized");
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            capabilities.setCapability("chrome.binary", "drivers/chromedriver");
            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
            driver = new ChromeDriver(options);
            driver.manage().window().maximize();
        } else if (browser.equalsIgnoreCase("Firefox")) {
            System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");
            driver = new FirefoxDriver();
        } else if (browser.equalsIgnoreCase("IE")) {
            System.setProperty("webdriver.ie.driver", "drivers/IEDriverServer.exe");
            driver = new InternetExplorerDriver();
        } else if (browser.equalsIgnoreCase("Edge") && OS.equalsIgnoreCase("windows 10")) {
            System.setProperty("webdriver.edge.driver", "drivers/MicrosoftWebDriver.exe");
            driver = new EdgeDriver();
        } else {
            log.warning("Browser/OS combo not supported");
            System.exit(0);
        }
        return driver;
    }

    @Parameters("browser")
    @BeforeMethod
    public void setup(String browser) {
        log.info("Browser: " + browser);
        // create driver
        driver = createDriver(browser);

        // Configure default wait times
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        if (!browser.equalsIgnoreCase("IE")) {
            driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        }
    }

    @AfterMethod(alwaysRun = true)
    public void stopDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

}