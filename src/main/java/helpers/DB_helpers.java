package helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DB_helpers extends Selenium_setup {

    private String driver = "com.mysql.jdbc.Driver";
    private String connectionString = "jdbc:mysql://localhost:3306/test";
    private String userName = "";
    private String password = "";


    public Connection connectToMySQL() {
        Connection con = null;
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(connectionString, userName, password);
        } catch (Exception e) {
            System.out.println(e);
        }
        return con;
    }

    public void selectQuery(String query) {
        Connection con = connectToMySQL();
        ResultSet rs;
        try {
            Statement stmt=con.createStatement();
            rs=stmt.executeQuery(query);
            while(rs.next())
                log.info(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}

