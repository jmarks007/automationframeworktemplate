# UIAutomationFrameworkTemplate
## Template for building mostly UI-automation frameworks

### Current set of features:
* POM design
* Support for major browsers including: Chrome, Firefox, IE 10/11, Edge
* DB validation helper methods to verify data gets set via UI
* An example Google.com page object and tests for it
* WebDriver-related helper methods such as: checking if elements are present, waiting for elements to be clickable, zooming in/out, scrolling up/down
* Example test runner .xml files that show you how to incorporate variables into tests

### How to install/use:
1. Click "Download repository" in (https://bitbucket.org/jmarks007/automationframeworktemplate/downloads/)
2. Unzip the downloaded repository where you want it
3. Open the pom.xml file in your favorite IDE
4. Under the "test runners" folder, you'll see some example runners that you can run